import 'package:flutter/material.dart';
import 'package:warta/screens/call.dart';
import 'package:warta/screens/home.dart';
import 'package:warta/screens/status.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class Firebase {
  static initializeApp({required options}) {}
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'warta',
      theme: ThemeData(useMaterial3: true, scaffoldBackgroundColor: Colors.white, primarySwatch: Colors.grey),
      debugShowCheckedModeBanner: false,
      home: MainScreen(),
    );
  }
}



// Main

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int currentPage = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text("warta",
          style: TextStyle(fontWeight: FontWeight.bold,
              fontSize: 28,
              fontFamily: 'RampartOne',
              letterSpacing: 8),
        ),
        leading: Padding(
          padding: const EdgeInsets.only(top: 5, left: 10),
          child: InkWell(onTap: (){
            _scaffoldKey.currentState!.openDrawer();
          } ,child: CircleAvatar(child: Text("A"),),
          ),
        ),
      ),
      body: <Widget>[
        HomeScreen(),
        StatusScreen(),
        CallScreen(),
      ][currentPage],
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                CircleAvatar(radius: 42,),
                SizedBox(width: 15,),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Ankit", style: TextStyle(fontWeight: FontWeight.bold),),
                    Text("+91 1234567890")
                  ],)
              ],
            )),
            ListTile(onTap: (){
              Navigator.pop(context);
            } ,title: Text("New Group"), leading: Icon(Icons.groups_outlined),),
            ListTile(onTap: (){} ,title: Text("Account"), leading: Icon(Icons.person),),
            ListTile(onTap: (){} ,title: Text("Notifications"), leading: Icon(Icons.notifications),),
            ListTile(onTap: (){} ,title: Text("Display"), leading: Icon(Icons.display_settings),),
          ],
        ),
      ),
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index){
          setState(() {
            currentPage = index;
          });
        },
        selectedIndex: currentPage,
        destinations: const [
          NavigationDestination(icon: Icon(Icons.chat_bubble_outline_rounded), selectedIcon: Icon(Icons.chat_bubble_rounded), label: "Chats"),
          NavigationDestination(icon: Icon(Icons.amp_stories_outlined), selectedIcon: Icon(Icons.amp_stories), label: "Status"),
          NavigationDestination(icon: Icon(Icons.call), label: "Calls"),
        ],
      ),
    );
  }
}
