import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 65,
          padding: const EdgeInsets.all(10.0),
          child: TextField(
            decoration: InputDecoration(
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30),
                borderSide: BorderSide.none,
              ),
              hintText: "Search...",
              hintStyle: TextStyle(fontSize: 14),
              prefixIcon: Icon(Icons.search_rounded),
              prefixIconColor: Colors.black45,
            ),
          ),
        ),
        Expanded(child: ListView.builder(itemBuilder: (BuildContext context, int index){
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              leading: CircleAvatar(radius: 30,),
              title: Text("Ankit"),
              subtitle: Text("last message sent"),
              trailing: Column(
                children: [
                  Text("16 July", style: TextStyle(fontSize: 10),),
                  const SizedBox(height: 8,),
                  CircleAvatar(radius: 12, child: Text("300", style: TextStyle(fontSize: 8),),)
                ],
              ),
            ),
          );
        }, itemCount: 10,))
      ],
    );
  }
}
