import 'package:flutter/material.dart';

class CallScreen extends StatefulWidget {
  const CallScreen({Key? key}) : super(key: key);

  @override
  State<CallScreen> createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(child: ListView.builder(itemBuilder: (BuildContext context, int index){
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListTile(
          leading: CircleAvatar(radius: 30,),
          title: Text("Ankit"),
          subtitle: Row(children: [
            Icon(Icons.call_made),
            const SizedBox(width: 8,),
            Text("07:36 16 July")
          ],),
          trailing: Icon(Icons.call),
        ),
      );
    }, itemCount: 10,));
  }
}
