import 'package:flutter/material.dart';

class StatusScreen extends StatefulWidget {
  const StatusScreen({Key? key}) : super(key: key);

  @override
  State<StatusScreen> createState() => _StatusScreenState();
}

class _StatusScreenState extends State<StatusScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Row(
            children: [
              Stack(
                children: [
                  CircleAvatar(radius: 40,),
                  Positioned(right: 0, bottom: 0,child: IconButton(icon: Icon(Icons.add_circle, size: 35,), onPressed: () {  },))
                ],
              ),
              const Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Text("Your Status", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
              )
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.all(15.0),
          child: Text("Updates"),
        ),
        Expanded(child: ListView.builder(itemBuilder: (BuildContext context, int index){
          return const Padding(
            padding: EdgeInsets.all(8.0),
            child: ListTile(
              leading: CircleAvatar(radius: 30,),
              title: Text("Ankit"),
              subtitle: Text("16 July"),
            ),
          );
        }, itemCount: 6,)),
      ],
    );
  }
}
